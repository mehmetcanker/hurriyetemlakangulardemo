angular.module('hurriyetApp', ['ui.router', 'ngAnimate', 'ngResource',
  'controllerModule', 'hurriyetDirective', 'hurriyetFilter'])

  .constant('HURRIYETEMLAKAPI', {
    URL: 'http://192.168.34.59:8080'
  })

  .run(['$rootScope', '$state', '$stateParams',
    function ($rootScope, $state, $stateParams) {
      $rootScope.$state = $state;
      $rootScope.$stateParams = $stateParams;
    }
  ]
)

  .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {


    $urlRouterProvider
      .when('/c?id', '/portfolio/:id')
      .when('/portfolio/:id', '/')
      .otherwise('/');


    $stateProvider
      .state("home", {
        url: "/",
        templateUrl: 'partials/home/home.html',
        controller: 'HomeController'
      })
      .state('portfolio', {
        url: '/portfolio',
        templateUrl: 'partials/portfolio/portfolio.html',
        controller: 'PortfolioController'
      })
      .state('extra', {
        url: '/extra/:param',
        templateUrl: 'partials/extra/extra.html',
        controller: 'ExtraController'
      })
      .state('parent', {
        //abstract:true,
        url: '/parent',
        templateUrl: 'partials/parent/parent.html',
        controller: 'ParentController'
      })
      .state('parent.child1', {
        url: '/child1',
        templateUrl: 'partials/parent/child/child-1.html',
        controller: 'Child1Controller'
      })
      .state('parent.child2', {
        url: '/child2',
        templateUrl: 'partials/parent/child/child-2.html',
        controller: 'Child2Controller'
      })

  }
  ]
);
