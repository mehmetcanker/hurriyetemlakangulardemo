angular.module('controllerModule', ['hurriyetService'])


  .controller('HomeController', function ($scope, getMessage, getNews, $state, $stateParams,PostExample) {
    console.log("HomeController");


    //PostExample.save({'slm':'slm'});
    //PostExample.delete({'slm':'slm'});


    getMessage.query(function (b) {

      console.log(b.message);
      $scope.messages = b.message;
    });

    getNews.query(function (b) {
      $scope.news = b.news;
    });

  })

  .controller('PortfolioController', function ($scope, getList, createPager, $filter, $timeout, $state) {
    console.log("PortfolioController");
    $scope.loading = true;
    var orderBy = $filter('orderBy');
    $scope.siralamaParametresi = 'name';
    $scope.reverse = false;
    $scope.params = {};


    getList($scope.params).query(function (e) {
      console.log(e);
      $scope.count = e.counter;
      $scope.pagerRules = createPager(e.counter);
      $scope.portfolioList = orderBy(e.list, $scope.siralamaParametresi, $scope.reverse);
    });


    $scope.sortByTag = function (param) {
      $scope.siralamaParametresi = param;
      $scope.reverse = !$scope.reverse;
      $scope.portfolioList = orderBy($scope.portfolioList, param, $scope.reverse);
    };

    $scope.sortingOption = function (param) {
      $scope.sort = $scope.sortingModel;
    }

    $scope.priceFilter = function () {
      if ($scope.$timeout) {
        $timeout.cancel($scope.$timeout);
      }
      $scope.timout = $timeout(function () {
        getList($scope.params).query(function (e) {
          $scope.count = e.counter;
          $scope.pagerRules = createPager(e.counter);
          $scope.portfolioList = orderBy(e.list, $scope.siralamaParametresi, $scope.reverse);
        });
      }, 1000);

    };

    $scope.search = function () {
      //console.log(typeof $scope.search.keyword =="undefined")
      if ($scope.search.keyword && $scope.search.price) {
        var query = {keyword: $scope.search.keyword, price: $scope.search.price};
        $state.go("portfolio", query, {inherit: false});
      }

    }


    $scope.anasayfayaGit = function () {
      $state.go("home", {'param': 'Selam'});

    };

  })

  .controller('ParentController', function ($scope) {


  })

  .controller('Child1Controller', function ($scope, $rootScope) {
    console.log("Child Scope1 ");
    console.log( $rootScope);


  })

  .controller('Child2Controller', function ($scope) {
    console.log("Child Scope2");


  }).controller('ExtraController', function ($scope, $state, $stateParams) {

    console.log($state)
    console.log($stateParams.param);

    $scope.param = $stateParams.param;

  })

