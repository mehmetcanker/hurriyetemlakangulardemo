angular.module('hurriyetService', ['ngResource'])

  .factory('getList', function ($resource, HURRIYETEMLAKAPI) {
    return function (params) {
      return $resource('http://demo0295264.mockable.io/listem', params, {
        query: {method: 'GET', isArray: false, cache: false}
      });

    };
  })
  .factory('getMessage', function ($resource, HURRIYETEMLAKAPI) {
    return $resource('http://demo0295264.mockable.io/message#', {}, {
      query: {method: 'GET', isArray: false, cache: false}
    });
  })

  .factory('getNews', function ($resource, HURRIYETEMLAKAPI) {
    return $resource('http://demo0295264.mockable.io/news#', {}, {
      query: {method: 'GET', isArray: false, cache: false}
    });
  })
  .factory('PostExample', function ($resource, HURRIYETEMLAKAPI) {
    return $resource("/api/posts/:id");
  })


  .factory('createPager', function () {
    return function (param) {
      var next = false;
      var arr = new Array();
      var counter = param;

      if (param > 12) {
        counter = 12;
        next = true;
      }
      for (var i = 0; i < counter; i++) {
        arr.push(i);
      }

      return {
        activeNextButton: next,
        buttonCount: arr
      }
    }

  });




