angular.module('hurriyetDirective', [])

  .directive('pagination', function () {
    return {
      restrict: 'E',
      templateUrl: 'partials/directive/pager.html',
      scope: {
        items: '@pagecount', activePageCount: '@activeNextButton',
        resultcount: '@resultcount', loading: '@loading'
      }
    }
  })
  .directive('loading', ['$http', function ($http) {
    return {
      restrict: 'A',
      link: function (scope, elem, attrs) {
        scope.isLoading = function () {
          return $http.pendingRequests.length > 0;
        };
        scope.$watch(scope.isLoading, function (v) {
          if (v) {
            angular.element(elem).css('display', 'block');
          } else {
            angular.element(elem).css('display', 'none');
          }
        });
      }
    };
  }])
  .directive('a', function () {
    return {
      restrict: 'E',
      link: function (scope, elem, attrs) {
        if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
          elem.on('click', function (e) {
            e.preventDefault();
          });
        }
      }
    };
  });

